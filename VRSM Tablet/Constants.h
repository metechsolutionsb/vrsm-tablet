//
//  Constants.h
//  VRSM
//
//  Created by Tracey on 7/14/14.
//  Copyright (c) 2014 Silverlake. All rights reserved.
//

#ifndef VRSM_Constants_h
#define VRSM_Constants_h

#define IOS7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define kWebServicesGeneralURL2 @"http://mobilityg303.ddns.eagleeyes.tw:88/vrsm/general"
#define kWebServicesSecureURL2 @"http://mobilityg303.ddns.eagleeyes.tw:88/vrsm/secure"
#define kWebServicesGeneralURL @"http://slakemobility.ddnsking.com:88/vrsm/general"
#define kWebServicesSecureURL @"http://slakemobility.ddnsking.com:88/vrsm/secure"
#define kWebServicesDigitalURL @"http://slakemobility.ddnsking.com:88/vrsm/digital"
#define kWebServicesMediaURL @"http://slakemobility.ddnsking.com:88/vrsm/media"
#define kWebServicesAdvertisingURL @"http://mobilityg303.ddns.eagleeyes.tw:88/ma/api/advert/list"
//#define kCryptoKey [[NSUserDefaults standardUserDefaults] objectForKey:@"VRSMUDID"]
#define kCryptoKey @"abcd"
#define kSessionKey [[NSUserDefaults standardUserDefaults] objectForKey:@"VRSMSessID"]
#define kOrderKey [[NSUserDefaults standardUserDefaults] objectForKey:@"VRSMOrderID"]

// ==============
// Theme
// ==============

// Button
#define kButtonRed 82
#define kButtonGreen 218
#define kButtonBlue 192
#define kButtonPressedRed 0
#define kButtonPressedGreen 209
#define kButtonPressedBlue 156
#define kTermsButtonRed 213
#define kTermsButtonGreen 61
#define kTermsButtonBlue 43
#define kGreenButtonBackground @"Button_Green_50.png"
#define kRedButtonBackground @"Button_Red_50.png"
#define kYellowButtonBackground @"Button_Yellow_50.png"
#define kGreenButtonBackground40 @"Button_Green_40.png"
#define kYellowButtonBackground40 @"Button_Yellow_40.png"
#define kRedButtonBackground40 @"Button_Red_40.png"
#define kCheckboxUnchecked @"Checkbox.png"
#define kCheckboxChecked @"Checkbox_Checked.png"
#define kCheckboxGreyUnchecked @"CheckboxGrey.png"
#define kCheckboxGreyChecked @"CheckboxGrey_Checked.png"

// Background Color
#define kBackgroundRed 255
#define kBackgroundGreen 104
#define kBackgroundBlue 83
#define kRedBackgroundRed 249
#define kRedBackgroundGreen 84
#define kRedBackgroundBlue 68
#define kGreyBackgroundRed 235
#define kGreyBackgroundGreen 235
#define kGreyBackgroundBlue 235

// Tab Bar
#define kTabBackgroundRed1 104
#define kTabBackgroundGreen1 53
#define kTabBackgroundBlue1 83
#define kTabBackgroundRed2 255
#define kTabBackgroundGreen2 193
#define kTabBackgroundBlue2 71
#define kTabBackgroundRed3 255
#define kTabBackgroundGreen3 104
#define kTabBackgroundBlue3 83
#define kTabBackgroundRed4 85
#define kTabBackgroundGreen4 212
#define kTabBackgroundBlue4 177
#define kTabBackgroundRed5 255
#define kTabBackgroundGreen5 122
#define kTabBackgroundBlue5 52
#define kTabIcon1 @"Icon_Search_20.png"
#define kTabIcon2 @"Icon_WishList_30.png"
#define kTabIcon3 @"Icon_Home_30.png"
#define kTabIcon4 @"Icon_Cart_30.png"
#define kTabIcon5 @"Icon_Profile_30.png"
#define kTabBackground @"Tab_Bar_320.png"
#define kTabIconSel1 @"Icon_Search_Sel_20.png"
#define kTabIconSel2 @"Icon_WishList_Sel_30.png"
#define kTabIconSel3 @"Icon_Home_Sel_30.png"
#define kTabIconSel4 @"Icon_Cart_Sel_30.png"
#define kTabIconSel5 @"Icon_Profile_Sel_30.png"
//#define kTabBackground @"Tab_Bar_320.png"

// Tool Bar
#define kToolBarBackgroundRed 249
#define kToolBarBackgroundGreen 84
#define kToolBarBackgroundBlue 68

#define kNavBarLogo @"Logo_60.png"

#define kLightGreyRed 204
#define kLightGreyGreen 204
#define kLightGreyBlue 204

#define kDarkGreenRed 70
#define kDarkGreenGreen 171
#define kDarkGreenBlue 139

#endif
