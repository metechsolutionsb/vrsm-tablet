//
//  DepartmentListViewController.h
//  VRSM Tablet
//
//  Created by Ridz on 4/27/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DepartmentListViewController : UITableViewController{
    NSArray *ladies;
    NSArray *men;
    NSArray *books;
    NSArray *electronics;
    NSArray *pets;
    NSArray *tops;
    NSDictionary *departmentListDictionary;
    NSArray *departmentListArray;
}

-(void) LoadDepartment : (NSString*) department;
@end
