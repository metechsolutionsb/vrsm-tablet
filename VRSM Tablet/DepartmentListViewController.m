//
//  DepartmentListViewController.m
//  VRSM Tablet
//
//  Created by Ridz on 4/27/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import "DepartmentListViewController.h"
#import <MFSideMenu/MFSideMenu.h>
#import "ProductListViewController.h"

@interface DepartmentListViewController ()

@end

@implementation DepartmentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) LoadDepartment : (NSString*) department
{
    ladies = [[NSArray alloc] initWithObjects:@"Tops", @"Skirts", @"Pants", @"Accessories", @"Sweaters", @"Shoes", @"Dresses", nil];
    men = [[NSArray alloc] initWithObjects:@"Shirts", @"Trousers", @"Shoes", nil];
    books = [[NSArray alloc] init];
    electronics = [[NSArray alloc] initWithObjects:@"Cameras", @"Smartphones", @"PCs", @"Laptops", @"Televisions", nil];
    pets = [[NSArray alloc] initWithObjects:@"Food", @"Toys", @"Beds & Houses", @"Shampoos", nil];
    tops = [[NSArray alloc] initWithObjects:@"Tank Tops", @"Shirts", @"T-Shirts", @"Cardigans", nil];
    
    departmentListDictionary = @{@"Ladies" : ladies,
                                 @"Men" : men,
                                 @"Electronic" : electronics,
                                 @"Pet" : pets,
                                 @"Top" : tops};
    departmentListArray = [departmentListDictionary objectForKey:[NSString stringWithFormat:@"%@",department]];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [departmentListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tableCell"];
    if( cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tableCell"];
    }
    
    NSString *brand = [departmentListArray objectAtIndex:indexPath.row];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.text = brand;
    [cell.textLabel setTextColor:[UIColor grayColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [self performSegueWithIdentifier:@"next" sender:self];
}

- (IBAction)toggleVisibility:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
//#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"next"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ProductListViewController *destViewController = [segue destinationViewController];
        destViewController.navigationItem.title = [NSString stringWithFormat:@"%@",[departmentListArray objectAtIndex:indexPath.row]];
        
    }
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    //UINavigationController *navigationController = segue.destinationViewController;
//    
}


@end
