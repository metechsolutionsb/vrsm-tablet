//
//  DismissSegue.m
//  VRSM Tablet
//
//  Created by Ridz on 4/28/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
