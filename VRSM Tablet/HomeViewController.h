//
//  HomeViewController.h
//  VRSM Tablet
//
//  Created by Ridz on 4/27/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

- (IBAction)toggleVisibility:(id)sender;
@end
