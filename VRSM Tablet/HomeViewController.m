//
//  HomeViewController.m
//  VRSM Tablet
//
//  Created by Ridz on 4/27/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import "HomeViewController.h"
#import <MFSideMenu.h>

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleVisibility:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
@end
