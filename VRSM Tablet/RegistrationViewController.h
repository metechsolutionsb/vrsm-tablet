//
//  RegistrationViewController.h
//  VRSM Tablet
//
//  Created by Ridz on 4/28/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *registerTable;
@end
