//
//  SubMenuViewController.h
//  VRSM Tablet
//
//  Created by Ridz on 4/27/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *menuSections;
    NSArray *topSectionTitles;
    NSArray *bottomSectionTitles;
    NSArray *departmentSection;
    int indexTag;
}

@property (strong, nonatomic) IBOutlet UIView *imgView;
@property (strong, nonatomic) IBOutlet UIView *imgDisplay;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *memberSince;
@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet UITableView *menu;
@end
