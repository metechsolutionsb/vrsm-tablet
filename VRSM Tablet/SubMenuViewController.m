//
//  SubMenuViewController.m
//  VRSM Tablet
//
//  Created by Ridz on 4/27/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import "SubMenuViewController.h"
#import <MFSideMenu/MFSideMenu.h>
#import "DepartmentListViewController.h"
#import "HomeViewController.h"
#import "NewsletterViewController.h"
#import "MerchantViewController.h"
#import "ContactUsViewController.h"
#import "TnCViewController.h"

@interface SubMenuViewController ()

@end

@implementation SubMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    menuSections = [[NSArray alloc] initWithObjects:@"VRSM", @"Department", @"More About VRSM", nil];
    topSectionTitles = [[NSArray alloc] initWithObjects:@"Home", @"Gallery", @"Shop By Brand", nil];
    bottomSectionTitles = [[NSArray alloc] initWithObjects:@"Newsletter", @"Augmented Reality", @"Merchant", @"Contact Us", @"Terms & Conditions", nil];
     departmentSection = [[NSArray alloc] initWithObjects:@"Ladies", @"Men", @"books", @"Electronic", @"Pet", @"Top", nil];
    
    [self.name setText:@"Brandon"];
    [self.memberSince setText:@"Member Since : 2014"];
    [self.location setText:@"Location : Selangor"];
    
    [self.view setBackgroundColor : [UIColor colorWithRed:kGreyBackgroundRed/255.0 green:kGreyBackgroundGreen/255.0 blue:kGreyBackgroundBlue/255.0 alpha:1.0]];
    [self.imgDisplay setBackgroundColor : [UIColor colorWithRed:kGreyBackgroundRed/255.0 green:kGreyBackgroundGreen/255.0 blue:kGreyBackgroundBlue/255.0 alpha:1.0]];
    self.imgView.layer.cornerRadius = 20;
    self.imgView.layer.masksToBounds = YES;
    [self.menu setBackgroundColor:[UIColor colorWithRed:55/255.0 green:47/255.0 blue:45/255.0 alpha:1.0]];
    self.menu.delegate = self;
    self.menu.dataSource = self;
    [self.menu registerClass:[UITableViewCell class] forCellReuseIdentifier:@"MenuCell"];
    [self.imgDisplay addSubview:self.imgView];
    [self.imgDisplay addSubview:self.name];
    [self.view addSubview:self.menu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView setSeparatorColor:[UIColor blackColor]];
    return 40.0;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (CGRect) offScreenFrame
{
    return CGRectMake( - self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height);
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.frame.size.width, 50)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:28.0f]];
    //[label setTintColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    NSString *string = [NSString stringWithFormat:@"%@",menuSections[section]];
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    
    if(section == 0)
    {
        [view setBackgroundColor:[UIColor colorWithRed:kTabBackgroundRed4/255.0 green:kTabBackgroundGreen4/255.0 blue:kTabBackgroundBlue4/255.0 alpha:1.0]];
    }
    else if (section == 1)
        [view setBackgroundColor:[UIColor colorWithRed:kTabBackgroundRed2/255.0 green:kTabBackgroundGreen2/255.0 blue:kTabBackgroundBlue2/255.0 alpha:1.0]];
    else
        [view setBackgroundColor:[UIColor colorWithRed:kTabBackgroundRed1/255.0 green:kTabBackgroundGreen1/255.0 blue:kTabBackgroundBlue1/255.0 alpha:1.0]];
    
    return view;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return topSectionTitles.count;
    else if (section == 1)
        return departmentSection.count;
    else
        return bottomSectionTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.backgroundColor = [UIColor colorWithRed:55/255.0 green:47/255.0 blue:45/255.0 alpha:1.0];
    if(indexPath.section == 0)
    {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", topSectionTitles[indexPath.row]];
        indexTag = indexPath.row;
    }else if (indexPath.section == 1)
    {
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[departmentSection objectAtIndex:indexPath.row]];
        indexTag = indexPath.row + topSectionTitles.count;
        
    }else
    {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", bottomSectionTitles[indexPath.row]];
        indexTag = indexPath.row + topSectionTitles.count + departmentSection.count;
    }
    [cell setTag:indexTag];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger vcIndex = [[tableView cellForRowAtIndexPath:indexPath] tag];
    
    UIViewController *demoViewController;
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        }else if(indexPath.row == 1){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"gallery"];
            demoViewController.title = [NSString stringWithFormat:@"%@", topSectionTitles[indexPath.row]];
        }else if (indexPath.row == 2){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"shopbybrand"];
            demoViewController.title = [NSString stringWithFormat:@"%@", topSectionTitles[indexPath.row]];
        }
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        
        NSArray *controllers = [NSArray arrayWithObject:demoViewController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }else if (indexPath.section == 1){
        [self performSegueWithIdentifier:@"registration" sender:self];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        /*DepartmentListViewController *demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"departmentList"];
        [demoViewController LoadDepartment:[departmentSection objectAtIndex:indexPath.row]];
        demoViewController.title = [NSString stringWithFormat:@"%@", departmentSection[indexPath.row]];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoViewController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];*/
    }else{
        if(indexPath.row == 0){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"newsletter"];
            demoViewController.title = [NSString stringWithFormat:@"%@", bottomSectionTitles[indexPath.row]];
        }else if(indexPath.row == 1){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AR"];
            demoViewController.title = [NSString stringWithFormat:@"%@", bottomSectionTitles[indexPath.row]];
        }else if(indexPath.row == 2){
           demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"merchant"];
            demoViewController.title = [NSString stringWithFormat:@"%@", bottomSectionTitles[indexPath.row]];
        }else if(indexPath.row == 3){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contactus"];
            demoViewController.title = [NSString stringWithFormat:@"%@", bottomSectionTitles[indexPath.row]];
        }else if(indexPath.row == 4){
            demoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tnc"];
            demoViewController.title = [NSString stringWithFormat:@"%@", bottomSectionTitles[indexPath.row]];
        }
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        
        NSArray *controllers = [NSArray arrayWithObject:demoViewController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
}
@end
