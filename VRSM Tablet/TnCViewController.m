//
//  TnCViewController.m
//  VRSM Tablet
//
//  Created by Ridz on 4/28/15.
//  Copyright (c) 2015 Ridz. All rights reserved.
//

#import "TnCViewController.h"
#import <MFSideMenu.h>

@interface TnCViewController ()

@end

@implementation TnCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"terms" ofType:@"html"];
     NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
     [self.webview loadHTMLString:htmlString baseURL:nil];
    
    [self.webview setScalesPageToFit:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)toggleVisibility:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
